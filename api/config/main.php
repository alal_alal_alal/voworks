<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),    
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'aliases' => [
        '@api' => dirname(dirname(__DIR__)) . '/api',
        '@common' => dirname(dirname(__DIR__)) . '/common',
    ],
    'components' => [
        'response' => [
            'format' => 'json',
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
                ]
            ]
        ],
        'request' => [

            'parsers' => [

                'application/json' => 'yii\web\JsonParser',

            ],

            'enableCookieValidation' => false,

            'enableCsrfValidation' => false,

        ],

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,

            'rules' => [
                [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['v1/goods'],
                    'prefix' => 'api',
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]
                ], [
                    'class' => \yii\rest\UrlRule::class,
                    'controller' => ['v1/category'],
                    'prefix' => 'api',

                ],
                '/api/v1/categories/one' => 'v1/category/one'

            ],        
        ]
    ],
    'params' => $params,
];



