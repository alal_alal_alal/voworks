<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "categories".
 *
 * @property int $id
 * @property string $name
 *
 * @property GoodsCategories[] $goodsCategories
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
    public function extraFields()
    {
        return ['goods'];
    }
    public function getGoods()
    {
        return $this->hasMany(Goods::class, ['id'=>'good_id'])->via('goodsCategories');
    }
    /**
     * Gets query for [[GoodsCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCategories()
    {
        return $this->hasMany(GoodsCategories::class, ['category_id' => 'id']);
    }
}
