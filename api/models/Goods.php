<?php

namespace api\models;

use Yii;

/**
 * This is the model class for table "goods".
 *
 * @property int $id ид
 * @property string $name имя
 * @property string $description описание
 * @property int $cost цена
 *
 * @property GoodsCategories[] $goodsCategories
 */
class Goods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description', 'cost'], 'required'],
            [['description'], 'string'],
            [['cost'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'cost' => 'Cost',
        ];
    }

    /**
     * Gets query for [[GoodsCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCategories()
    {
        return $this->hasMany(GoodsCategories::class, ['good_id' => 'id']);
    }
    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id'=>'category_id'])->via('goodsCategories');
    }
    public function extraFields()
    {
        return ['categories'];
    }
}
