<?php

namespace api\modules\v1\controllers;

use api\models\Categories;
use api\models\Category;
use api\models\Goods;
use common\models\User;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

/**
 * Country Controller API
 *
 *
 */
class CategoryController extends BasicController
{
    public $modelClass = '\api\models\Category';

    /**
     * @throws NotFoundHttpException
     */
    public function actionOne($id)
    {
        $model = Category::findOne($id);
        if (!$model)
            throw  new NotFoundHttpException('invalid id');
        return $model->goods;

    }
}



