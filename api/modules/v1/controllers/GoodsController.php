<?php

namespace api\modules\v1\controllers;

use api\models\Categories;
use api\models\Goods;
use common\models\User;
use yii\filters\auth\HttpBasicAuth;
use yii\rest\ActiveController;

/**
 * Country Controller API
 *
 *
 */
class GoodsController extends BasicController
{
    public $modelClass = '\api\models\Goods';


    public function behaviors()
    {
        return [
            'basicAuth' => [
                'class' => \yii\filters\auth\HttpBasicAuth::className(),
                'auth' => [$this, 'auth']
            ],
        ];
    }

    function auth($username, $password)
    {
        $user = User::find()->where(['username' => $username])->one();
        if ($user && $user->validatePassword($password)) {
            return $user;
        }
        return null;
    }


}



