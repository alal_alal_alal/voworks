<?php

namespace common\tests\unit\models;


use api\models\Category;
use api\models\Goods;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * Login form test
 */
class GoodsTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;


    /**
     * @return array
     */


    public function testGoodsIsCorrectSave()
    {
        $model = new Goods([
            'name' => 'test good',
            'description' => 'test descr',
            'cost' => 1
        ]);

        expect('good was saved', $model->save())->true();
    }

    public function testGoodsCorrectSaveWithCategory()
    {
        $good = new Goods([
            'name' => 'test good',
            'description' => 'test descr',
            'cost' => 1
        ]);
        $category = new Category([
            'name' => 'test category',
        ]);

        expect('good was saved', $good->save())->true();
        expect('category was saved', $category->save())->true();
        expect('category was linked', $good->link('categories', $category));
        expect('good has category', ArrayHelper::getColumn( $good->categories, 'id'))->contains($category->id);
    }


}
